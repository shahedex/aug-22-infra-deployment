resource "kubernetes_deployment_v1" "backend" {
  metadata {
    name = "backend"
    labels = {
      test = "MyExampleApp"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        test = "MyExampleApp"
      }
    }

    template {
      metadata {
        labels = {
          test = "MyExampleApp"
        }
      }

      spec {
        container {
          image = "shahedmehbub/tf-two-tier-backend:${var.backend_tag}"
          name  = "backend"

          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service_v1" "backend_svc" {
  metadata {
    name = "backend"
  }
  spec {
    selector = {
      test = kubernetes_deployment_v1.backend.metadata.0.labels.test
    }
    
    port {
      port        = 5000
      target_port = 5000
    }

    type = "LoadBalancer"
  }
}
